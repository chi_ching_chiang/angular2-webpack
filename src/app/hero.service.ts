import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { Http,Headers } from '@angular/http';

// import { HEROS } from './mock-heroes';
@Injectable()

export class HeroService {
    constructor(private http:Http){}
    private heroesurl = 'app/heroes';
    private headers = new Headers({'Content-Type':'application/json'});
    getHero(id:number):Promise<Hero>{
        return this.getHeroes().then(heroes => heroes.find(hero => hero.id === id));
    }
    getHeroes(): Promise<Hero[]> {
        //toPromise是由rxjs加載進來,get方法回傳一Obserable物件中並無該方法,目的是將Obserable物件轉為Promise物件
        return this.http.get(this.heroesurl).toPromise().then(res => res.json().data as Hero[]);
        // return Promise.resolve(HEROS);
    }
    getHeroesSlowly(): Promise<Hero[]> {
        return new Promise<Hero[]>(resolve =>
            setTimeout(resolve, 2000)) // delay 2 seconds
            .then(() => this.getHeroes());
    }
    update(hero:Hero):Promise<Hero>{
        const url = `${this.heroesurl}/${hero.id}`;
        return this.http.put(url,JSON.stringify(hero),{headers:this.headers}).toPromise().then(()=>hero ).catch(this.handleError);
    }
    create(name:string):Promise<Hero>{
        return this.http.post(this.heroesurl,JSON.stringify({name:name}),{headers:this.headers}).toPromise().then(res => res.json().data).catch(this.handleError);
    };
    delete(id:number):Promise<void>{
        let url = `${this.heroesurl}/${id}`;
        return this.http.delete(url,{headers:this.headers}).toPromise().then(()=>null).catch(this.handleError);
    }
    private handleError(error:any):Promise<any>{
        console.log(error);
        return Promise.reject(error.message || error);
    }
}