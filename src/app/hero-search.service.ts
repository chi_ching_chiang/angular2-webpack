import { Injectable } from '@angular/core';
import { Http,Response } from '@angular/http';
import { Observable } from 'rxjs';

import {Hero} from './hero';
@Injectable()

export class HeroSearchService{
    constructor(private http:Http){}
    search(term:string):Observable<Hero[]>{
        //  app/heroes為hero.service的對應路徑;name為Hero的屬性,透過angular/http get方法取資料,返回值得型別為Observable,透過get方法取資料,
        //返回值的型別為Observable,原始的Response物件包覆在其中.透過Observable提供的map方法輪巡Observable取出Response物件,
        //r.json()回傳一Object型別的物件,內有data屬性,屬性值為一物件陣列,用as Hero[]語法在內部轉為Hero物件陣列,再依照Observable<Hero[]>
        //型別傳出search方法
        return this.http.get(`app/heroes/?name=${term}`).map((r:Response) => r.json().data as Hero[]).catch(this.handleError);
    }
    private handleError(error:any):Promise<any>{
        console.log(error);
        return Promise.reject(error);
    }
}
