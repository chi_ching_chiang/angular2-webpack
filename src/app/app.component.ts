import { Component } from '@angular/core';
import { TestService } from './test.service';
@Component({
    selector:'my-app',
    templateUrl:'app.component.html',
    providers:[TestService] 
})
export class AppComponent{
    title:String = 'Tour of Heroes [Webpack]';
}