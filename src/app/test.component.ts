import { Component,Input,Output, OnInit,EventEmitter} from '@angular/core';
import { Hero } from './hero';
import { TestService } from './test.service';

@Component({
    moduleId: module.id,
    selector: 'test',
    template:`<div (click)="onClick($event)">click me</div> {{hero.name}}`
})
export class TestComponent implements OnInit {
    constructor(private testService : TestService) {
         
    }
    num : number = 0;
    ngOnInit() { }
    /*
        元件間通訊方式(between hero-detail.component(parent) and test.component(child)) :
        1、Parent -> Child 使用@Input裝飾子
        2、Child -> Parent 使用@Output裝飾子
            參數代表template tag中，屬性等號(=)的左側，ex : <test [hero2]="hero" (customEvent)="clickEventHandler($event)"></test>
        3、創造一Service元件(test.service.ts),由根組件(app.component.ts)或根模組(app.module.ts)注入
        ,以保持單一實例;父子兩端將service匯入,一端註冊為change事件subscribe，一端進行
        emit,可以此做到廣播效果
    */
    @Input('hero2') hero:Hero;
    @Output() customEvent = new EventEmitter();
    onClick():void{
        let n = this.num++;
        this.testService.change.emit(n)
        this.customEvent.emit(n);
    }
}