import { Component,Input,OnInit } from '@angular/core';
import { ActivatedRoute,Params} from '@angular/router';
import { HeroService } from './hero.service';
import {Hero} from './hero';
import { TestService } from './test.service';

@Component({
    moduleId:module.id,
    selector: 'my-hero-detail',
    templateUrl:'hero-detail.component.html',
    styleUrls:['hero-detail.component.css'] 
  
})

export class HeroDetailComponent implements OnInit {
    hero : Hero;
    count : number;
    constructor(private heroService:HeroService,private router:ActivatedRoute,private testService : TestService){
        this.testService.change.subscribe((value : number) =>{
            console.log(`click ${value} times`);
        })
    }
    ngOnInit():void{
        this.router.params.forEach((params:Params) =>{
            let id = +params['id']; //+號表示 string -> integer
            this.heroService.getHero(id).then(hero => this.hero = hero);
        })

    }
    goBack():void{
        window.history.back();
    }
    save():void{
        this.heroService.update(this.hero).then((hero)=> this.goBack());
    }
    clickEventHandler(count:number):void{
        
        this.count = count;
    }
}