import { Component,OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Hero} from './hero';
import { HeroService} from './hero.service';
@Component({
    moduleId:module.id,
    selector :'my-dashboard',
    templateUrl:'dashboard.component.html',
    styleUrls:['dashboard.component.css']
})
export class DashboardComponent implements OnInit{
    heroes : Hero[] = [];
    constructor(private heroservice : HeroService,private router : Router){}
    ngOnInit():void{
        this.heroservice.getHeroes().then(heroes => this.heroes = heroes.slice(1,5));
    }
    gettoDetail(hero:Hero):void{
        let link = ['/detail',hero.id];
        this.router.navigate(link);
    }
}