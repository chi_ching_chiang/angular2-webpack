// Angular
import '@angular/core';
import '@angular/common';
import '@angular/compiler';
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/http';
import '@angular/router';
import '@angular/forms';
import 'angular2-in-memory-web-api';
// RxJS
import 'rxjs';

// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...

 