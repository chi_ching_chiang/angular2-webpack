#angular 2 + webpack + livereload

###Quick start

# clone or download the repo
$ git clone https://chi_ching_chiang@bitbucket.org/chi_ching_chiang/angular2-webpack.git

# change directory to your app
$ cd angular2-webpack

# install the dependencies with npm
$ npm install

# if you want to build development code :
$ npm run dev

# if you want to build production codes :
$ npm run prod

** Note : 所有跟Typescript有關的檔案均統一放在src/app中，靜態檔案(index.html、jpg、png、css...)放在src/public裡。Typescript的程式進入點為main.ts，若要重新命名，config/webpack.common.js裡第12行要跟著改。
### Important!
@angular 2.0.0
node_modules/@angular/compiler/bundles/compiler.umd.js : 13350 有bug,請找到該檔案13350行處自行修改
https://github.com/angular/angular/issues/11590

# original : 
function _split(uri) {return uri.match(_splitRe);}

# fix : 
function _split(uri) {return uri.toString().match(_splitRe);}